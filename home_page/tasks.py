#!/usr/bin/env python
from colorama import Back, Fore, Style
import luigi
from luigi.contrib import ssh
from sarge import get_both


class GenericTask(luigi.Task):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.venv = ["cd", "/home/pi/.virtualenvs/homepage", "&&",
                     "source", "bin/activate", "&&",
                     "source", "bin/postactivate", "&&"]
        self.remote = ssh.RemoteContext("raspberry1")

    def action_log(self, cmd):
        # Print the command in question
        print(Back.BLUE + Fore.WHITE + " ".join(cmd))
        print(Style.RESET_ALL)

    def output_log(self, output, err=False):
        if not isinstance(output, str):
            output = output.decode('utf-8')

        COLOR = Fore.GREEN
        if err:
            COLOR = Fore.WHITE + Back.RED

        for _ in output.split("\n"):
            if _:
                print(COLOR + (_))
        print(Style.RESET_ALL)

    def exec_local(self, cmd):
        self.action_log(cmd)

        stdout, stderr = get_both(" ".join(cmd))

        self.output_log(stdout)
        self.output_log(stderr, err=True)

    def exec_remote(self, cmd):
        self.action_log(cmd)

        output = self.remote.check_output(self.venv + cmd)

        self.output_log(output)


class GitPush(GenericTask):
    #  luigi --module tasks GitPush --local-scheduler
    def run(self):
        self.exec_local(["git", "push", "origin", "-q"])


class Deploy(GenericTask):
    #  luigi --module tasks Deploy --local-scheduler
    option = luigi.Parameter(default="full")

    def run(self):
        self.exec_remote(["git", "pull"])

        if self.option == "full":
            # If option is "full" new installs or migrations needed
            self.exec_remote(
                ["pip", "install", "-r", "requirements/production.txt"])
            self.exec_remote(
                ["django-admin", "migrate"])
            self.exec_remote(
                ["django-admin", "collectstatic", "--noinput"])


class Restart(GenericTask):
    #  luigi --module tasks Restart --local-scheduler
    def run(self):
        self.exec_remote(
            ["sudo", "supervisorctl", "restart", "homepage"])


if __name__ == "__main__":
    import sys

    if len(sys.argv) > 1:
        option = sys.argv[1]
        luigi.build([GitPush(), Deploy(option=option), Restart()],
                    local_scheduler=True)
    else:
        luigi.build([GitPush(), Deploy(), Restart()], local_scheduler=True)
