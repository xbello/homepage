from django.contrib import admin
from .models import Skill, SocialLink, UserProfile


admin.site.register(UserProfile)
admin.site.register(SocialLink)
admin.site.register(Skill)
