from django.contrib.auth.models import User
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView, RedirectView

from .models import Skill, SocialLink, UserProfile


class RedirectView(RedirectView):
    permanent = False
    url = reverse_lazy("home:user", kwargs={"username": "xbello"})


class IndexView(DetailView):
    model = UserProfile
    slug_field = "user__username"
    slug_url_kwarg = "username"
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = {}

        if self.object:
            context["social_links"] = \
                {_.name: _ for _ in SocialLink.objects.filter(
                    user=self.object.user)}
            context["user"] = self.object.user

        context.update(kwargs)

        return super().get_context_data(**context)

    def get_queryset(self):
        qs = super().get_queryset().select_related("user")

        return qs


class SkillsListView(ListView):

    """View to the Skills of an user."""

    model = Skill
    template_name = "skill_list.html"

    def get_context_data(self, **kwargs):
        context = {}

        context["user"] = get_object_or_404(
            User, username=self.kwargs.get("username"))

        context.update(kwargs)

        return super().get_context_data(**context)

    def get_queryset(self):
        qs = super().get_queryset().select_related("user")

        return qs.filter(user__username=self.kwargs["username"])
