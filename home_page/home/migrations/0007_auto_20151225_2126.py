# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0006_skill_user'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='skill',
            name='to_date',
        ),
        migrations.AddField(
            model_name='skill',
            name='level',
            field=models.CharField(default='LO', choices=[('LO', 'Low'), ('AV', 'Average'), ('HI', 'High')], max_length=2),
        ),
    ]
