# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0002_sociallink'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociallink',
            name='name',
            field=models.CharField(default='null', max_length=20),
            preserve_default=False,
        ),
    ]
