# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_auto_20151225_2126'),
    ]

    operations = [
        migrations.AlterField(
            model_name='skill',
            name='level',
            field=models.IntegerField(default=0, help_text='A value between 0 and 100.'),
        ),
    ]
