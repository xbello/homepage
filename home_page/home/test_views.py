from datetime import date
from os import path

from django.conf import settings
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string
from django.test import override_settings

from utils.testing import BaseCase

from . import models


@override_settings(MEDIA_ROOT=path.join(path.dirname(__file__), "test_files/"))
class testHomePage(BaseCase):

    def test_home_page_redirects(self):
        resp = self.c.get(reverse("home_redirect"),
                          follow=True)

        self.assertEqual(resp.status_code, 200)

    def test_home_page_renders_the_right_template(self):
        resp = self.c.get(reverse("home:user", kwargs={"username": "xbello"}))
        self.assertEqual(resp.status_code, 200)

        # Test the top bar
        with self.assertTemplateUsed("top_bar.html"):
            self.assertInHTML("<title>X. Bello home page</title>",
                              resp.content.decode())
            self.assertInHTML(
                '<a href="{}"><i class="fa fa-mortar-board fa-lg">'.format(
                    reverse("home:skills", kwargs={"username": "xbello"})) +
                '</i>Skills</a>', resp.content.decode(), count=1)
            self.assertInHTML(
                '<a href="#"><i class="fa fa-wrench fa-lg"></i>Experience</a>',
                resp.content.decode(), count=1)
            self.assertInHTML(
                '<a href="#"><i class="fa fa-book fa-lg"></i>Blog</a>',
                resp.content.decode(), count=1)
            self.assertInHTML(
                '<a href="#"><i class="fa fa-envelope fa-lg"></i>Contact</a>',
                resp.content.decode(), count=1)

            self.user.age = date.today().year - self.year
            social_links = {_.name: _ for _ in models.SocialLink.objects.filter(
                user=self.user)}
            self.assertEqual(
                resp.content.decode(),
                render_to_string("index.html",
                                 context={"userprofile": self.user_profile,
                                          "user": self.user,
                                          "social_links": social_links}))

            self.assertIn("bootstrap.min.css", resp.content.decode())

    def test_home_page_renders_the_content(self):
        resp = self.c.get(
            reverse("home:user", kwargs={"username": "xbello"}))
        self.assertEqual(resp.status_code, 200)

        self.assertTemplateUsed(resp, "index.html")

    def test_correct_user_data(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": "xbello"})).content.decode()

        self.assertIn("20", resp)
        self.assertIn("Rue del Percebe, nº1", resp)
        self.assertIn("test@test.com", resp)
        self.assertIn("+34-123456879", resp)

    def test_correct_social_footer(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": "xbello"}))

        self.assertTemplateUsed(resp, "social_footer.html")

    def test_correct_social_links(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": "xbello"})).content.decode()

        social_links = {_.name: _ for _ in models.SocialLink.objects.filter(
            user=self.user)}
        self.assertEqual(
            resp,
            render_to_string("index.html",
                             context={
                                 "userprofile": self.user_profile,
                                 "user": self.user,
                                 "social_links": social_links}))
        self.assertIn("https://github.com/xbello",
                      resp)

    def test_social_links_are_exclusive_per_user(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": self.user2.username})).content.decode()

        social_links = {_.name: _ for _ in models.SocialLink.objects.filter(
            user=self.user2)}
        self.assertEqual(
            resp,
            render_to_string("index.html",
                             context={
                                 "userprofile": self.user_profile2,
                                 "user": self.user2,
                                 "social_links": social_links}))

    def test_profile_pic_is_shown(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": self.user.username})).content.decode()

        self.assertIn(path.join(settings.MEDIA_ROOT, "pics/profile.jpg"),
                      resp)

    def test_profile_pic_is_shown_for_anonymous(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": self.user2.username})).content.decode()

        self.assertIn(static("img/anonymous.png"), resp)


class TestSkillsPage(BaseCase):
    def test_home_page_has_link_to_skills(self):
        resp = self.c.get(
            reverse("home:user",
                    kwargs={"username": self.user.username})).content.decode()

        self.assertIn(reverse("home:skills",
                              kwargs={"username": self.user.username}),
                      resp)

    def test_the_list_of_skills_is_shown(self):
        url = reverse("home:skills",
                      kwargs={"username": self.user.username})
        resp = self.c.get(url).content.decode()

        self.assertEqual(
            resp,
            render_to_string("skill_list.html",
                             context={
                                 "user": self.user,
                                 "object_list": [self.skill1, self.skill],
                                 "userprofile": self.user_profile}))

        self.assertIn("Python", resp)
        self.assertIn("FORTRAN", resp)
        # Skills for other users doesn't appear
        self.assertNotIn("Go", resp)
