from datetime import date

from django.test import TestCase

from model_mommy import mommy


class testUserProfile(TestCase):
    def setUp(self):
        super().setUp()
        self.year = 1995
        self.user = mommy.make("home.UserProfile",
                               user__first_name="Xabi",
                               user__last_name="Bello",
                               user__username="xbello",
                               user__email="test@test.com",
                               birth=date(self.year, 1, 1),
                               address="Rue del Percebe, nº1",
                               phone="+34-123456879")

    def tearDown(self):
        self.user.delete()
        super().tearDown()

    def test_user_profile_string_repr(self):
        self.assertEqual(str(self.user), "X. Bello profile")

    def test_user_profile_get_age(self):
        self.assertEqual(date.today().year - self.year, self.user.get_age())


class testSocialLink(TestCase):
    def setUp(self):
        super().setUp()
        self.sociallink = mommy.make("home.SocialLink",
                                     user__username="xbello",
                                     name="github",
                                     url="https://github.com/xbello")

    def tearDown(self):
        self.sociallink.delete()
        super().tearDown()

    def test_sociallink_string_repr(self):
        self.assertEqual(str(self.sociallink), "Github link for xbello")


class testSkill(TestCase):
    def setUp(self):
        super().setUp()
        self.skill = mommy.make("home.Skill",
                                user__username="xbello",
                                name="Python",
                                description="Programming in python a lot",
                                from_date="2006-01-01",
                                level=90)

    def tearDown(self):
        self.skill.delete()
        super().tearDown()

    def test_skill_string_repr(self):
        self.assertEqual(str(self.skill), "Python skill for xbello")
