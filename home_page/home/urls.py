from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^(?P<username>\w+)/$', views.IndexView.as_view(), name="user"),
    url(r'^(?P<username>\w+)/skills/$',
        views.SkillsListView.as_view(), name="skills")
]
