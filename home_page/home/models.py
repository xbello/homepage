from datetime import date
from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    birth = models.DateField()
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=15)
    pic = models.ImageField(upload_to="pics/", blank=True)

    def get_age(self):
        return relativedelta(date.today(), self.birth).years

    def __str__(self):
        return "{}. {} profile".format(self.user.first_name[:1],
                                       self.user.last_name)


class Skill(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    level = models.IntegerField(default=0,
                                help_text="A value between 0 and 100.")
    from_date = models.DateField()

    def __str__(self):
        return "{} skill for {}".format(self.name, self.user)

    class Meta:
        ordering = ["level"]


class SocialLink(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=20)
    url = models.URLField()

    def __str__(self):
        return "{} link for {}".format(self.name.title(),
                                       self.user.username)
