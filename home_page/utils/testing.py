from datetime import date
from os import path

from django.conf import settings
from django.test import Client, TestCase

from model_mommy import mommy


class BaseCase(TestCase):
    def setUp(self):
        super().setUp()
        self.year = 1995
        self.user = mommy.make("User",
                               username="xbello",
                               email="test@test.com")
        self.user2 = mommy.make("User")
        self.user_profile = mommy.make(
            "home.UserProfile",
            user=self.user,
            birth=date(self.year, 1, 1),
            pic=path.join(settings.MEDIA_ROOT, "pics/profile.jpg"),
            address="Rue del Percebe, nº1",
            phone="+34-123456879")
        self.user_profile2 = mommy.make("home.UserProfile",
                                        user=self.user2)
        self.facebook_link = mommy.make("home.SocialLink",
                                        user=self.user,
                                        name="github",
                                        url="https://github.com/xbello")
        self.google_link = mommy.make("home.SocialLink",
                                      user=self.user,
                                      url="http://plus.google.es/")
        self.skill = mommy.make("home.Skill",
                                user=self.user,
                                name="FORTRAN",
                                from_date=date(1970, 1, 1),
                                level=90)
        self.skill1 = mommy.make("home.Skill",
                                 user=self.user,
                                 name="Python",
                                 from_date=date(2000, 1, 1),
                                 level=50)
        self.skill2 = mommy.make("home.Skill",
                                 user=self.user2,
                                 name="Go",
                                 from_date=date(2015, 1, 1),
                                 level=10)
        self.c = Client()

    def tearDown(self):
        self.skill.delete()
        self.skill1.delete()
        self.skill2.delete()
        self.facebook_link.delete()
        self.google_link.delete()
        self.user_profile.delete()
        self.user_profile2.delete()
        self.user2.delete()
        self.user.delete()
        super().tearDown()
