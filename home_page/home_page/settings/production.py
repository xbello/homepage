from .base import *
import os


DEBUG = False
TEMPLATE_DEBUG = DEBUG

SECRET_KEY = os.environ["SECRET_KEY"]

ALLOWED_HOSTS = os.environ.get(
    'ALLOWED_HOSTS', 'xbello.herokuapp.com,xbello.eu')\
    .split(',')

ADMINS = (
    ('Xabi Bello', 'xbello@gmail.com'),
)

MANAGERS = ADMINS

STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'
