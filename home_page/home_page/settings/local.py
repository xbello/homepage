from .base import *


ADMINS = (
    ('Xabi Bello', 'xbello@gmail.com'),
)

MANAGERS = ADMINS

INSTALLED_APPS += ("debug_toolbar",)

# To test the Cache
# MIDDLEWARE_CLASSES += (
#     'django.middleware.cache.UpdateCacheMiddleware',
#     'django.middleware.cache.FetchFromCacheMiddleware',
# )
