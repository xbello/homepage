from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

from home import views


urlpatterns = patterns(
    '',
    url(r'^$', views.RedirectView.as_view(), name='home_redirect'),
    url(r'^u/', include('home.urls', namespace="home")),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) +\
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
