from django.test import LiveServerTestCase

from selenium import webdriver


class test_HomePage(LiveServerTestCase):
    def setUp(self):
        self.b = webdriver.Firefox()
        self.b.implicitly_wait(5)
        super().setUp()

    def tearDown(self):
        super().tearDown()
        self.b.quit()

    def test_user_can_reach_the_home_page(self):
        self.fail("Complete this test")
        self.b.get(self.live_server_url + "/")
